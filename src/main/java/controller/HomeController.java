/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

/**
 *
 * @author ryan
 */
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
 
@Controller
@RequestMapping("/home.htm")
public class HomeController {
 
    @RequestMapping(method = RequestMethod.GET)
    public String home(ModelMap modelMap) {
        System.out.println("on home method");
        modelMap.put("printme", "Home !!");
        return "home";
    }
}