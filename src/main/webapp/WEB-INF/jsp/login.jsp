<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Welcome to Spring Web MVC project</title>
        <link href="<c:url value="/resources/theme1/css/bootstrap.min.css" />" rel="stylesheet">
        <link href="<c:url value="/resources/theme1/css/main.css" />" rel="stylesheet">
        <script src="<c:url value="/resources/theme1/js/bootstrap.min.js" />"></script>
    </head>
    <body>
        <div class="content col-sm-12">
            <h1>Welcome back!</h1>
            <form:form class="form-horizontal" method="POST" commandName="account" action="process.htm">
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Email</label>
                    <div class="col-sm-10">
                        <form:input type="number" class="form-control" path="id" placeholder="Id"/>
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputPassword3" class="col-sm-2 control-label">Password</label>
                    <div class="col-sm-10"> 
                        <form:input type="password" class="form-control" path="password" placeholder="Password"/>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                        <button type="submit" value="Save" class="btn btn-default">Sign in</button>
                    </div>
                </div>
            </form:form>
        </div>
    </body>
</html>