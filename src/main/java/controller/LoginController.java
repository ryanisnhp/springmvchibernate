/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

/**
 *
 * @author ryan
 */
import model.User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
 
@Controller
//@RequestMapping("/login")
public class LoginController {
    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public String login(ModelMap modelMap) {
        modelMap.addAttribute("account", new User());
        return "login";
    }
    @RequestMapping(value = "/process", method = RequestMethod.POST)
    public String process(@ModelAttribute(value = "account")User user, ModelMap modelMap){
        if (user.getPassword().length() > 7){
            modelMap.put("account", user);
            return "home";
        }
        return "login";
    }
}